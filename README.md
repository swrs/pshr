<img src="lib/pshr.png" width="375">

# Pshr

*Pshr* is a Rails engine to facilitate file uploads using the (great) [Shrine](https://github.com/shrinerb/shrine) gem, providing some common written and rewritten functionalities and configurations, as so often is the case. *Pshr* works with [*pshr-js*](https://gitlab.com/swrs/pshr-js) which provides all necessary styles and javascript.

## Requirements

- [Rails](https://github.com/rails/rails) >= 6.0.3.3
- [Sidekiq](https://github.com/mperham/sidekiq) >= 6.1
- [Redis](https://github.com/redis/redis)
- [libvips](https://github.com/libvips/libvips) >= 8.9.2

## Installation

Currently *Pshr* is not on RubyGems and installed via git:

```ruby
gem "pshr", git: "https://gitlab.com/swrs/pshr.git", branch: "2.0.0"
```

Install [*pshr-js*](https://gitlab.com/swrs/pshr-js) see [*pshr-js*](https://gitlab.com/swrs/pshr-js) docs.

Mount *Pshr* in host's `routes.rb`:

```ruby
Rails.application.routes.draw do
  # …
  mount Pshr::Engine => "/pshr"
  # …
end
```

Add necessary secrets to the application credentials:

```bash
$ EDITOR="my favourite editor" rails credentials:edit
```

```yaml
shrine:
  derivation_endpoint_secret: <YOUR_SECRET>
```

## Development

To develop *Pshr* you'll most likely need a running *Redis* server and *Sidekiq*. You can have `redis-server` installed or spin up a docker container, *Redis*' default port is `6379`. `sidekiq` has to be run from the dummy app's root.

```bash
$ cd pshr
$ redis-server
# or using docker
# $ docker run --rm -p 6379:6379 redis:6.0.9-alpine
$ rails s
$ cd test/dummy && bundle exec sidekiq
```

## Usage

- [Model and uploader](#model)
- [Associations](#associations)
- [Ranking](#ranking)
- [Controller and routes](#controller)
- [File validation](#file-validation)
- [File processing](#file-processing)
- [Test helpers](#test-helpers)

<a name="model"></a>
### Model and Uploader

*Pshr* comes with an `Uploadable` module to hook into your favourite model together with the `FileUploader`. For a comprehensive guide regarding the uploader setup look at the [Shrine](https://github.com/shrinerb/shrine) docs.

The uploader requires a `*_data` column of type `jsonb` or `text` (in this case `file_data`). When using the `Uploadable` module, you'll also need to install *Pshr*'s migrations in order to make use of `Uploadable`'s associations (see [Associations](#associations)) and a `row_order` column for the record's position.

```bash
$ rails pshr:install:migrations
$ rails db:migrate
```

```ruby
# db/migrate/*_create_uploads.rb
class CreateUploads < ActiveRecord::Migration[6.0]
  def change
    create_table :uploads do |t|
      t.text :file_data
      t.integer :row_order

      t.timestamps
    end
  end
end
```

Hook the `Uploadable` module and `FileUploader` into your model, or use your own uploader.

```ruby
# app/models/upload.rb
class Upload < ApplicationRecord
  include Pshr::Uploadable
  include Pshr::FileUploader::Attachment(:file) # could be :foo or :whatever your column is named
end

# or use your own uploader
class Upload < ApplicationRecord
  include Pshr::Uploadable
  include MyUploader::Attachment(:file)
end

# app/uploaders/my_uploader.rb
class MyUploader < Pshr::FileUploader
  # …
end

# or code your own uploader from scratch, inheriting from 
# Shrine instead of Pshr::FileUploader
class MyUploader < Shrine
  # …
end
```

<a name="associations"></a>
### Associations

You will probably want to associate uploads to other records like a `Post`. *Pshr* comes with a setup for many-to-many associations which allows an upload to be associated to multiple records and vice-versa. These many-to-many associations will be made through the `Pshr::Assignment` model and can be set up using `Pshr::Uploadable` and `Pshr::Assignable`.

As described above, install *Pshr*'s migrations if you haven't done so.

```bash
$ rails pshr:install:migrations
$ rails db:migrate
```

A `Pshr::Assignment` record has `uploadable_id`, `assignable_type`, and `assignable_id`. Thus it can plug into a model using `Pshr::Uploadable` one one side and `Pshr::Assignable` on the other, like this…

```
Post  --- Pshr::Assignment        --- Upload
id: 1     assignable_type: "Post"     id: 1
…         assignable_id: 1            file_data: …
          uploadable_id: 1
```

```ruby
class Post < ApplicationRecord
  include Pshr::Assignable
end
```

```ruby
class Upload < ApplicationRecord
  include Pshr::Uploadable
  include Pshr::FileUploader::Attachment(:file)

  has_many :posts, through: :assignments, source: :assignable, source_type: "Post"
  # could be appended by
  # has_many :apples, through: :assignments, source: :assignable, source_type: "Apple"
  # …
end
```

Now we can associate `Upload`s to `Post`s and vice-versa…

```ruby
post = Post.create
upload = Upload.create(file: File.open("/path/to/file"))

post.uploads << upload

upload.posts
#=> <ActiveRecord::Associations::CollectionProxy [#<Post id: 1, created_at: "2020-11-03 17:20:29", updated_at: "2020-11-03 17:20:29">]>
post.uploads
#=> <ActiveRecord::Associations::CollectionProxy [#<Upload id: 1, file_data: "…", row_order: 0, title: nil, created_at: "2020-11-03 17:20:29", updated_at: "2020-11-03 17:20:29">]>

upload.posts.first == post #=> true
post.uploads.first == upload #=> true
```

<a name="ranking"></a>
### Ranking

Records by models using `Pshr::Uploadable` and `Pshr::Assignable` can be ranked using [*RankedModel*](https://github.com/mixonic/ranked-model) in order to change to records' order. This ranking can be done on all `Upload`s or `Upload`s within the range of an association.

```ruby
one, two, three = 3.times.map { Upload.create(file: File.open("/path/to/file")) }

Upload.ranked #=> uploads ranked by :row_order

one.row_order_rank #=> 0
two.row_order_rank #=> 1

two.update row_order_position: :first
two.reload.row_order_rank #=> 0
```

In order to rank within the range of an associated record, there is sort of a *Gotcha*: the ranking is done on the records of `Pshr::Assignment`.

```ruby
post_one, post_two = 2.times.map { Post.create }
post_one.uploads << one # assoc. upload one twith post_one
post_two.uploads << [two, three] # assoc. upload two and three with post_two

post_one.uploads.first == one #=> true
post_two.uploads.first == two #=> true
post_two.uploads.second == three #=> true

# now we'll rank post_two.uploads.second first
# by updating the rank of the assignment between post_one and upload three
post_two.assignments.find_by(uploadable_id: two.id).update row_order_position: :first
post_two.uploads.first == two #=> false
post_two.uploads.first == three #=> true
```

<a name="controller"></a>
### Controller, routes, and views

To make use of frontend uploads you can use the `Pshr::UploadsController` and some views / partials. Build your uploads controller, inherit from `Pshr::UploadsController` and set up some routes.

```ruby
# app/config/routes.rb
Rails.application.routes.draw do
  # … 

  # set the "resource" param to the name of your upload model "Upload", "MyUpload", or "Whatever"
  resources :uploads, defaults: { resource: "Upload" }
end

# app/controllers/uploads_controller.rb
class UploadsController < Pshr::UploadsController
end
```

The above setup will work, using *Pshr*'s views. In order to use your own views, you can override the views at `app/views/pshr/uploads/*.html.erb`.

A basic form to generate an upload with optional associations is:

```erb
<%= form_with model: upload, local: true do |form| %>

  <% if form.object.errors.any? %>
    <%= form.object.errors.size %> <%= "error".pluralize(form.object.errors.size) %> occured:
    <ul>
      <% form.object.errors.full_messages.each do |message| %>
        <li><%= message %></li>
      <% end %>
    </ul>
  <% end %>

  <# form.fields_for are used to establish associations via Pshr::Assignment %>
  <%= form.fields_for :assignments do |assignment_form| %>
    <%= assignment_form.hidden_field :assignable_type %>
    <%= assignment_form.hidden_field :assignable_id %>
  <% end %>

  <%= form.hidden_field :file, value: form.object.cached_file_data %>
  <%= form.file_field :file %>

  <%= form.submit "#{form.object.new_record? ? 'Create' : 'Update'} upload" %>
<% end %>
```

<a name="file-validation">File Validation</a>
### File Validation

To validate files use [Shrine's validation](https://shrinerb.com/docs/plugins/validation) in your used uploader. Though `Pshr::FileUploader` comes with prebaked validations for mime-types and a maximum filesize. To make use of those validations add the respective instance methods to your model (the one including the uploader).

```ruby
class Upload < ApplicationRecord
  include Pshr::Uploadable
  include Pshr::FileUploader::Attachment(:file)

  # return an array of allowed mime-types
  def valid_mime_types
    %w(image/png image/gif application/pdf)
  end

  # return an integer of bytes
  def valid_max_size
    5 * 1024 * 1024 # equals to 5 MB
  end
end
```

<a name="file-processing"></a>
### File processing

Shrine comes with many great solutions to handle file processing, on-the-fly using the [Derivation Endpoint](https://shrinerb.com/docs/plugins/derivation_endpoint) or upfront using [Derivatives](https://shrinerb.com/docs/plugins/derivatives). *Pshr* comes with some setup to use file processing.

#### Derivation Endpoint

*Pshr* mounts a derivation endpoint at `pshr/derivations` which is set up in `config/initializers/shrine.rb`. All file processing is handled in the background using *Sidekiq*.

*NOTE: In case you did not mount *Pshr* at `/pshr` and you want to make use of the derivation endpoint, make sure to override the `prefix` setting of the `derivation_endpoint` plugin in an initializer.*

`Pshr::FileUploader` has a `:thumb` derivation:

```erb
<%= image_tag @upload.file.derivation_url(:thumb, 640) %>
```

To add your own derivation:
```ruby
class Pshr::FileUploader < Shrine

  derivation :my_image do |file, width|
    # …
  end
end
```

```erb
<%= image_tag @upload.file.derivation_url(:my_image, 1920) %>
```

#### Derivatives

Upfront file processing is handled by derivatives which have to be set up additionally in `Pshr::FileUploader` or yours. The processing of derivatives is triggered by the `Pshr::DerivativesJob`.

```ruby
class Pshr::FileUploader < Shrine
  # …

  Attacher.derivatives do |original|
    vips = ImageProcessing::Vips.source(original)

    {
      large: vips.resize_to_limit!(1440, nil),
      medium: vips.resize_to_limit!(1024, nil),
      small: vips.resize_to_limit!(375, nil),
    }
  end
end
```

<a name="test-helpers"></a>
### Test Helpers

*Pshr* comes with a couple of test helpers to facilitate testing upload models, controllers, … . The test helpers have to be required and included in your test files. See all available test helpers at `test/test_helpers/pshr`.

```ruby
require "test_helper"
require "test_helpers/test_case_helpers/file"

class UploadTest < ActiveSupport::TestCase
  include TestCaseHelpers::File
  
  test "has file" do
    @file = tempfile("test/fixtures/files/image.png")  
    @upload = Upload.new(file: @file)

    assert_not @upload.file.nil?
  end
end
```

```ruby
require "test_helper"
require "test_helpers/integration_test_helpers/file"

class UploadsControllerTest < ActionDispatch::IntegrationTest
  include IntegrationTestHelpers::File

  test "creates upload" do
    @file = uploaded_file("path/to/file.jpeg", "image/jpeg")

    assert_difference "Upload.count", 1 do
      post uploads_path, params: { upload: { file: @file } }
    end
  end
end
```

## Credits

- [Icon](lib/pshr.png) by Lucas Lejeune
