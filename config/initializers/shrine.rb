require "shrine"
require "shrine/storage/file_system"
require "shrine/storage/memory"

# shrine storages
if Rails.env.test?
  storages = {
    cache: Shrine::Storage::Memory.new,
    store: Shrine::Storage::Memory.new,
    derivatives: Shrine::Storage::Memory.new,
  }
else
  storages = {
    cache: Shrine::Storage::FileSystem.new("public", prefix: "uploads/cache"),
    store: Shrine::Storage::FileSystem.new("public", prefix: "uploads"),
    derivatives: Shrine::Storage::FileSystem.new("public", prefix: "uploads/derivatives"),
  }
end

Shrine.storages = storages

# basics plugins
Shrine.plugin :activerecord
Shrine.plugin :cached_attachment_data
Shrine.plugin :restore_cached_data

# file types plugins
Shrine.plugin :determine_mime_type, analyzer: :marcel
Shrine.plugin :type_predicates, mime: :marcel

# file handling plugins
Shrine.plugin :upload_options, cache: { move: true }, store: { move: true }

# validation
Shrine.plugin :validation
Shrine.plugin :validation_helpers

# processing plugins
Shrine.plugin :derivatives, storage: :derivatives
Shrine.plugin :derivation_endpoint, secret_key: Rails.application.credentials[:shrine][:derivation_endpoint_secret], 
  prefix: "pshr/derivations", upload: true, upload_location: -> {
  File.join("derivations", "#{File.basename(source.id, '.*')}-#{[name, *args].join('-')}#{File.extname(source.id)}")
}

# background jobs plugins
Shrine.plugin :backgrounding

# delegate promotion / deletion to background jobs
Shrine::Attacher.promote_block do
  Pshr::PromoteJob.perform_async(self.class.name, record.class.name, record.id, name, file_data)
end

Shrine::Attacher.destroy_block do
  Pshr::DestroyJob.perform_async(self.class.name, data)
end

# endpoint for frontend uploads
Shrine.plugin :upload_endpoint
