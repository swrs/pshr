Pshr::Engine.routes.draw do
  # endpoint for uploads
  mount Pshr::FileUploader.upload_endpoint(:cache) => "upload"
  
  # endpoint for on-the-fly processing
  mount Pshr::FileUploader.derivation_endpoint => "derivations"
end
