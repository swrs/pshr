module Pshr
  module TestHelpers
    module IntegrationTest
      module File

        def uploaded_file(path, mime_type = nil)
          mime_type = Marcel::MimeType.for(path) unless mime_type
          Rack::Test::UploadedFile.new(path, mime_type)
        end
      end
    end
  end
end
