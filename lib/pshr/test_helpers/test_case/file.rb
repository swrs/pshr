module Pshr
  module TestHelpers
    module TestCase
      module File

        # returns a Tempfile from a source file at path
        def tempfile(path)
          filename = ::File.basename(path, ".*") # filename without extension
          extension = ::File.extname(path) # file extension
          temp = Tempfile.new([filename, extension])

          ::File.open(path) do |file|
            temp.write(file.read)
          end

          temp.rewind
          temp
        end
      end
    end
  end
end
