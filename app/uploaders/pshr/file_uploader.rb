class Pshr::FileUploader < Shrine

  # file validations
  Attacher.validate do
    if record.respond_to?(:valid_mime_types) && !record.valid_mime_types.nil? 
      validate_mime_type_inclusion record.valid_mime_types
    end

    if record.respond_to?(:invalid_mime_types) && !record.invalid_mime_types.nil?
      validate_mime_type_exclusion record.invalid_mime_types
    end

    if record.respond_to?(:valid_max_size) && !record.valid_max_size.nil? 
      validate_max_size record.valid_max_size
    end
  end

  # derivations for on-the-fly processing
  derivation :thumb do |file, width|
    mime_type = Marcel::MimeType.for file

    pipeline = ImageProcessing::Vips
      .source(file)

    pipeline = pipeline.convert("jpg") if mime_type == "image/gif"

    pipeline
      .saver(quality: 50)
      .resize_to_limit!(width.to_i, nil)
  end
end
