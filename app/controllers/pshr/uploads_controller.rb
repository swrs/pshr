module Pshr
  class UploadsController < ::ApplicationController
    before_action :set_association, :set_resources
    before_action :set_resource, except: [:index]

    def index
      render File.join(resource_views_path, "index")
    end

    def new
      render File.join(resource_views_path, "new")
    end

    def create
      @resource.assign_attributes(resource_params)

      if @resource.save
        # upload was sucessfully created, redirect to index
        flash.now[:success] = "Upload created"
        redirect_to main_app.url_for(controller: params[:controller], action: "index")
      else
        # upload creation failed, render new
        flash.now[:error] = "Error creating upload"
        render File.join(resource_views_path, "new"), status: :unprocessable_entity
      end
    end

    def edit
      render File.join(resource_views_path, "edit")
    end

    def update
      if @resource.update(resource_params)
        flash.now[:success] = "Upload edited"

        respond_to do |format|
          format.html do 
            redirect_to main_app.url_for(controller: params[:controller], action: "index")
          end

          format.json { render body: nil, status: :ok }
        end
      else
        flash.now[:error] = "Error updating upload"
        render File.join(resource_views_path, "edit"), status: :unprocessable_entity
      end
    end

    def destroy
      if @resource.destroy
        flash.now[:success] = "Upload was deleted"
        redirect_to main_app.url_for(controller: params[:controller], action: "index")
      end
    end

    private

    # build ressource based on params[:ressource]
    def build_resource
      params[:resource].constantize
    end

    # set resource and optional assignment for association 
    def set_resource
      if !params[:id].blank?
        @resource ||= build_resource.find(params[:id])
      else
        @resource ||= build_resource.new

        if @association
          @resource.pshr_attachments
            .build(attacher_type: @association.class.to_s, attacher_id: @association.id)
        end
      end
    end

    def set_resources
      @resources = build_resource.all
    end

    # set association if attacher_type and _id are given
    def set_association
      if !params[:attacher_type].blank? && !params[:attacher_id].blank?
        @association = params[:attacher_type].constantize
          .find_by(id: params[:attacher_id])
      end
    end

    # permitted params for upload
    # including nested attributes for many-to-many attachment associations
    def resource_params
      params.require(build_resource.to_s.underscore.to_sym)
        .permit(:file, :row_order_position, pshr_attachments_attributes: [
          :id, :attacher_type, :attacher_id, :row_order_position, :_destroy
        ])
    end

    # root directory for views
    def resource_views_path
      "pshr/uploads" 
    end
  end
end
