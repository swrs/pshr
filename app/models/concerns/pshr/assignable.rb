module Pshr
  module Assignable
    extend ActiveSupport::Concern

    included do
      has_many :assignments, dependent: :destroy, 
        class_name: "Pshr::Assignment", as: :assignable 
      
      # is associated to uploads through assignments using Pshr::Assignment
      # uploads are ordered by assignments' default_scope
      has_many :uploads, through: :assignments
    end
  end
end
