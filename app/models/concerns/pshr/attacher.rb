module Pshr
  module Attacher
    extend ActiveSupport::Concern

    class_methods do

      # class method to add associations to other classes through Pshr::Attachment table
      # klasses can be a class or an array of classes
      def attacher_to(klasses, pshr_attachments_class_name: "Pshr::Attachment")
        klasses = [klasses] unless klasses.is_a?(Array)
        klasses.each do |klass|
          association_name = klass.to_s.underscore.pluralize.gsub("/", "_").to_sym

          has_many :pshr_attachments, dependent: :destroy, 
            class_name: pshr_attachments_class_name, as: :attacher

          has_many association_name, through: :pshr_attachments, source: :attached, 
            source_type: klass.to_s
        end
      end
    end
  end
end
