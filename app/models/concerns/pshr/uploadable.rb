module Pshr
  module Uploadable
    extend ActiveSupport::Concern

    # return array of valid mime-types
    def valid_mime_types
      nil
    end

    # return valid max filesize as integer
    def valid_max_size
      nil
    end
  end
end
