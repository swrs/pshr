module Pshr
  module Attached
    extend ActiveSupport::Concern

    class_methods do

      # class method to add associations to other classes through Pshr::Attachment table
      # klasses can be a class or an array of classes
      def attached_to(klasses, pshr_attachments_class_name: "Pshr::Attachment")
        klasses = [klasses] unless klasses.is_a?(Array)
        klasses.each do |klass|
          association = klass.to_s.underscore.pluralize.gsub("/", "_").to_sym

          has_many :pshr_attachments, dependent: :destroy, 
            class_name: pshr_attachments_class_name, as: :attached

          accepts_nested_attributes_for :pshr_attachments, reject_if: :all_blank,
            allow_destroy: true

          has_many association, through: :pshr_attachments, source: :attacher, 
            source_type: klass.to_s

          after_update :touch_pshr_attachments
        end
      end
    end

    # position attachment row order
    def position(record, position)
      attachment = pshr_attachments
        .find_by(attacher_type: record.class.to_s, attacher_id: record.id) 

      raise ActiveRecord::RecordNotFound unless attachment
      attachment.update(row_order_position: position) if attachment
    end

    private

    def touch_pshr_attachments
      self.transaction do
        pshr_attachments.touch_all
      end
    end
  end
end
