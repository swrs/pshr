module Pshr
  class Assignment < ApplicationRecord
    belongs_to :upload, foreign_key: :uploadable_id, inverse_of: :assignments
    belongs_to :assignable, polymorphic: true

    include RankedModel
    ranks :row_order, with_same: [:assignable_type, :assignable_id]

    # scope :ranked, -> { order(row_order: :asc) }
    default_scope { order(row_order: :asc) }

    before_create :ensure_association_uniqueness
    after_update :touch_assignable
    after_touch :touch_assignable

    private

    def touch_assignable
      self.assignable.touch
    end

    def ensure_association_uniqueness
      if self.class.find_by(assignable_type: self.assignable_type, 
          assignable_id: self.assignable_id, uploadable_id: self.uploadable_id)
        throw :abort
      end
    end
  end
end
