module Pshr
  class Attachment < ApplicationRecord
    belongs_to :attacher, polymorphic: true, inverse_of: :pshr_attachments, touch: true
    belongs_to :attached, polymorphic: true, inverse_of: :pshr_attachments, touch: true

    include RankedModel
    ranks :row_order, with_same: [:attacher_type, :attacher_id]

    default_scope { order(row_order: :asc) }

    before_create :ensure_uniqueness
    after_update :touch_attacher
    after_touch :touch_attacher

    private

    def ensure_uniqueness
      attachment = self.class
        .find_by(attacher_type: attacher_type, attacher_id: attacher_id,
        attached_type: attached_type, attached_id: attached_id)

      throw :abort if attachment
    end

    def touch_attacher
      attacher.touch
    end
  end
end
