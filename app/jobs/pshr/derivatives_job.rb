module Pshr
  class DerivativesJob
    include Sidekiq::Worker

    # create derivatives for attachment
    def perform(attacher_class, record_class, record_id, name, file_data)
      attacher_class = Object.const_get(attacher_class)
      record = Object.const_get(record_class).find(record_id)

      attacher = attacher_class.retrieve(model: record, name: name, file: file_data)
      attacher.create_derivatives
      attacher.atomic_persist
    rescue Shrine::AttachmentChanged, ActiveRecord::RecordNotFound => e
      # delete orphaned derivatives
      attacher&.destroy_attached 
    end
  end
end
