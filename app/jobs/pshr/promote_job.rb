module Pshr
  class PromoteJob
    include Sidekiq::Worker

    # promote attachment
    def perform(attacher_class, record_class, record_id, name, file_data)
      attacher_class = Object.const_get(attacher_class)
      record = Object.const_get(record_class).find(record_id)

      attacher = attacher_class.retrieve(model: record, name: name, file: file_data)
      attacher.atomic_promote

      ::Pshr::DerivativesJob.perform_async(attacher_class, record_class, record_id, name, record.reload.file_data)
    rescue Shrine::AttachmentChanged, ActiveRecord::RecordNotFound
      # attachment has changed or record not found
    end
  end
end
