require "test_helper"
require "pshr/test_helpers/test_case/file"

class DerivativesUploadTest < ActiveSupport::TestCase
  include Pshr::TestHelpers::TestCase::File
  # turn off Rails transactional tests to work properly with Shrine file promotion
  # use DatabaseCleaner gem to clean up db instead
  self.use_transactional_tests = false

  setup do
    DatabaseCleaner.start

    @upload = DerivativesUpload.new
    @upload.file = File.open(tempfile(file_fixture("image.jpg"))) 
  end

  teardown do
    DatabaseCleaner.clean
  end

  test "creates derivatives" do
    @upload.save

    assert_not @upload.reload.file_derivatives.empty?,
      "file derivatives hash is not empty"
  end

  test "is invalid with excluded mime-type" do
    @upload.file = tempfile(file_fixture("image.png"))
    assert_not @upload.valid?
  end
end
