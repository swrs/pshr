require "test_helper"
require "pshr/test_helpers/test_case/file"

class PostTest < ActiveSupport::TestCase
  include Pshr::TestHelpers::TestCase::File
  self.use_transactional_tests = false

  setup do
    DatabaseCleaner.start

    @post = Post.create
    @upload = Upload.create(file: tempfile(file_fixture("image.png")))
  end

  teardown do
    DatabaseCleaner.clean
  end

  test "has many pshr attachments" do
    assert @post.pshr_attachments.empty?
  end

  test "has many uploads" do
    @post.uploads << @upload 
    assert_includes @post.uploads, @upload
  end

  test "destroys associated assignments" do
    @post.uploads << @upload

    assert_difference "Pshr::Attachment.count", -1 do
      @post.destroy
    end
  end

  test "ranks associated uploads" do
    @one = @upload
    @two = Upload.create(file: tempfile(file_fixture("image.png")))
    @post.uploads = [@one, @two]

    assert_equal @one, @post.uploads.first,
      "one ranks first"

    @two.position(@post, :first)

    assert_equal @two, @post.reload.uploads.first, 
      "previously last ranks first"
  end

  test "post is touched by upload update" do
    updated_at = @post.updated_at
    @post.uploads << @upload
    sleep 1
    @upload.update file: tempfile(file_fixture("image.jpg"))
    assert_not_equal updated_at, @post.reload.updated_at
  end

  test "post is touched by attachment update" do
    updated_at = @post.updated_at
    @post.uploads << @upload
    sleep 1
    @post.pshr_attachments.first.update row_order_position: :first
    assert_not_equal updated_at, @post.reload.updated_at
  end

  test "post is touched by pshr_attachment destroy" do
    @post.uploads << @upload
    updated_at = @post.updated_at
    sleep 1
    @post.pshr_attachments.first.destroy
    assert_not_equal updated_at, @post.reload.updated_at
  end
end
