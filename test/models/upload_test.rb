require "test_helper"
require "pshr/test_helpers/test_case/file"

class UploadTest < ActiveSupport::TestCase
  include Pshr::TestHelpers::TestCase::File
  self.use_transactional_tests = false

  setup do
    DatabaseCleaner.start

    @upload = Upload.new
    @upload.file = tempfile(file_fixture("image.png")) 
  end

  teardown do
    DatabaseCleaner.clean
  end
  
  test "is valid" do
    assert @upload.valid?
  end

  test "is invalid with wrong mime-type" do
    @upload.file = tempfile(file_fixture("image.gif"))
    assert_not @upload.valid?
  end

  test "is invalid with wrong filesize" do
    @upload.file = tempfile(file_fixture("big.png"))
    assert_not @upload.valid?
  end

  test "promotes file to :store" do
    @upload.save
    assert_equal :store, @upload.reload.file.storage_key
  end

  test "has many posts through attachments" do
    @upload.save
    @post = Post.create

    assert_difference "Pshr::Attachment.count", 1 do
      @upload.posts << @post
    end

    assert_includes @upload.posts, @post
  end

  test "destroys associated attachments" do
    @upload.save
    @upload.posts << Post.create

    assert_difference "Pshr::Attachment.count", -1 do
      @upload.destroy
    end
  end

  test "duplicate attachment to same association raises exception" do
    @upload.save
    @post = Post.create
    @upload.posts << @post

    assert_raise ActiveRecord::RecordNotSaved do
      @upload.posts << @post
    end
  end

  test "ranks within many-to-many association" do
    one, two = 2.times.map { Upload.create(file: tempfile(file_fixture("image.png"))) } 
    post_one, post_two = 2.times.map { Post.create }
    post_one.uploads << [one, two]
    post_two.uploads << one

    assert_equal one, post_one.uploads.first, 
      "one is first upload of post_one"

    two.position(post_one, :first)

    assert_equal two, post_one.reload.uploads.first, "two is first upload of post_one"

    assert_equal post_one.pshr_attachments.find_by(attached_id: two.id).row_order_rank,
      post_two.pshr_attachments.find_by(attached_id: one.id).row_order_rank,
      "post_one upload two and post_two upload one rank equally"
  end
end
