# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_02_08_145131) do

  create_table "derivatives_uploads", force: :cascade do |t|
    t.text "file_data"
    t.integer "row_order"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "posts", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "pshr_attachments", force: :cascade do |t|
    t.string "attached_type"
    t.integer "attached_id"
    t.string "attacher_type"
    t.integer "attacher_id"
    t.integer "row_order"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["attached_type", "attached_id"], name: "index_pshr_attachments_on_attached_type_and_attached_id"
    t.index ["attacher_type", "attacher_id"], name: "index_pshr_attachments_on_attacher_type_and_attacher_id"
  end

  create_table "uploads", force: :cascade do |t|
    t.text "file_data"
    t.integer "row_order"
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
