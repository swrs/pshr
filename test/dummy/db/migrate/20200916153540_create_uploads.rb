class CreateUploads < ActiveRecord::Migration[6.0]
  def change
    create_table :uploads do |t|
      t.references :uploadable, polymorphic: true
      t.text :file_data
      t.integer :row_order
      t.string :title

      t.timestamps
    end
  end
end
