class RemoveUploadableFromUploads < ActiveRecord::Migration[6.0]
  def change
    remove_column :uploads, :uploadable_type
    remove_column :uploads, :uploadable_id
  end
end
