class CreateDerivativesUploads < ActiveRecord::Migration[6.0]
  def change
    create_table :derivatives_uploads do |t|
      t.text :file_data
      t.integer :row_order

      t.timestamps
    end
  end
end
