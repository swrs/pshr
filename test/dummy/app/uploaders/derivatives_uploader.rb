class DerivativesUploader < Pshr::FileUploader

  Attacher.derivatives do |file|
    pipeline = ImageProcessing::Vips.source(file)

    { small: pipeline.resize_to_limit!(100, 100) }
  end
end
