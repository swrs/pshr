class Upload < ApplicationRecord
  include Pshr::Uploadable
  include Pshr::FileUploader::Attachment(:file)

  include Pshr::Attached
  attached_to Post

  def valid_mime_types
    %w(image/png image/jpg image/jpeg)
  end

  def valid_max_size
    10 * 1024
  end
end
