class DerivativesUpload < ApplicationRecord
  include Pshr::Uploadable
  include DerivativesUploader::Attachment(:file)

  has_many :posts, through: :assignments, 
    source: :assignable, source_type: "Post"

  def invalid_mime_types
    %w(image/png)
  end
end
