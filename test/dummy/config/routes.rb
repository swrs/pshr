Rails.application.routes.draw do
  mount Pshr::Engine => "/pshr"

  # set uploads resources
  resources :uploads, defaults: { resource: "Upload" }
  resources :derivatives_uploads, defaults: { resource: "DerivativesUpload" }

  root to: "uploads#index"
end
