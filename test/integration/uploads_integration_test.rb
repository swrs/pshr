require "test_helper"
require "pshr/test_helpers/integration_test/file"

class UploadsIntegrationTest < ActionDispatch::IntegrationTest
  include Pshr::TestHelpers::IntegrationTest::File
  self.use_transactional_tests = false

  setup do
    DatabaseCleaner.start

    file = file_fixture("image.png")
    @upload = Upload.create(file: uploaded_file(file, Marcel::MimeType.for(file)))

    @post = Post.create
  end

  teardown do
    DatabaseCleaner.clean
  end

  test "index of all uploads" do
    get "/uploads"
    assert_response :success
  end

  test "new upload" do
    get "/uploads/new"
    assert_response :success
  end

  test "new upload within post" do
    get "/uploads/new?attacher_type=#{@post.class.to_s}&attacher_id=#{@post.id}"
    assert_response :success
    assert_select "h1", Regexp.new("Post #{@post.id}")
  end

  test "create upload" do
    file = file_fixture("image.jpg")

    assert_difference "Upload.count", 1 do
      post "/uploads", params: { upload: {
        file: uploaded_file(file, Marcel::MimeType.for(file))
      }}
    end

    assert_redirected_to "/uploads"
  end

  test "create invalid upload fails" do
    file = file_fixture("text.txt")

    assert_no_difference "Upload.count" do
      post "/uploads", params: { upload: {
        file: uploaded_file(file, Marcel::MimeType.for(file))
      }} 
    end

    assert_response 422
  end

  test "create upload with attachment" do
    file = file_fixture("image.jpg")

    assert_difference "Pshr::Attachment.count", 1 do
      post "/uploads", params: { upload: {
        file: uploaded_file(file, Marcel::MimeType.for(file)),
        pshr_attachments_attributes: [{ 
          attacher_type: @post.class.to_s, attacher_id: @post.id 
        }]
      }}
    end
  end

  test "creation rejects empty attachments" do
    assert_no_difference "Pshr::Attachment.count" do
      post "/uploads", params: { upload: { 
        file: uploaded_file(file_fixture("image.png"), "image/png"),
        pshr_attachments_attributes: [{ attacher_type: nil, attacher_id: nil }] 
      }}
    end
  end

  test "edit upload" do
    get "/uploads/#{@upload.id}/edit"
    assert_response :success
  end

  test "updates upload" do
    url = @upload.file.url
    file = file_fixture("image.jpg")

    patch upload_path(id: @upload.id), params: { 
      upload: { file: uploaded_file(file, Marcel::MimeType.for(file)) } 
    }

    assert_not_equal @upload.reload.file.url, url, "@upload.file.url has changed"
    assert_redirected_to uploads_path
  end

  test "update invalid upload" do
    file = uploaded_file(file_fixture("text.txt"))
    patch  "/uploads/#{@upload.id}", params: { upload: { file: file } }
    assert_response :unprocessable_entity
  end

  test "destroys upload" do
    assert_difference "Upload.count", -1 do
      delete "/uploads/#{@upload.id}"
    end

    assert_redirected_to uploads_path
  end

  test "destroy associated attachments" do
    @post = Post.create
    @post.uploads << @upload

    assert_difference "Pshr::Attachment.count", -1 do
      delete "/uploads/#{@upload.id}"
    end

    assert_not @post.reload.uploads.any?
  end

  test "ranks uploads within associated attachment as json" do
    one, two = 2.times.map { 
      Upload.create(file: uploaded_file(file_fixture("image.png"), "image/png")) 
    }

    @post = Post.create
    @post.uploads << [one, two]
    
    assert_equal one, @post.uploads.first,
      "one ranks first in @post.uploads"

    attachment = @post.pshr_attachments.find_by(attached_id: two.id)

    patch "/uploads/#{two.id}", 
      params: { format: :json, upload: { 
        pshr_attachments_attributes: [{ id: attachment.id, row_order_position: :first }] 
      }}, xhr: true

    assert_equal two, @post.reload.uploads.first,
      "two ranks first in @post.uploads"
  end

  test "destroys upload attachment" do
    @post.uploads << @upload

    attachment = @upload.pshr_attachments.find_by(attacher_type: @post.class.to_s,
      attacher_id: @post.id)

    assert_difference "Pshr::Attachment.count", -1 do
      patch upload_path(id: @upload.id), params: { upload: { pshr_attachments_attributes: 
        [{ id: attachment.id, _destroy: 1 }] } } 
    end
  end
end
