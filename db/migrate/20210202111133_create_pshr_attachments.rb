class CreatePshrAttachments < ActiveRecord::Migration[6.0]
  def change
    create_table :pshr_attachments do |t|
      t.references :attached, polymorphic: true
      t.references :attacher, polymorphic: true
      t.integer :row_order
      t.timestamps
    end
  end
end
