$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "pshr/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "pshr"
  spec.version     = Pshr::VERSION
  spec.authors     = ["swrs"]
  spec.email       = ["mail@pbernhard.com"]
  spec.homepage    = "https://gitlab.com/swrs/pshr"
  spec.summary     = "File upload engine for Rails."
  spec.description = spec.summary
  spec.license     = "MIT"

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 7.0"
  spec.add_dependency "shrine", "~> 3.3", ">= 3.3.0"
  spec.add_dependency "marcel", "~> 1.0"
  spec.add_dependency "image_processing", "~> 1.11", ">= 1.11.0"
  spec.add_dependency "sidekiq", "~> 7.2"
  spec.add_dependency "ranked-model", "~> 0.4", ">= 0.4.6"

  spec.add_development_dependency "sqlite3"
end
